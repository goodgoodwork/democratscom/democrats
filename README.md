# Democrats.com Wordpress Core

This repository is the seed that creates the Wordpress installation for Democrats.com

## Getting Started

Clone this repo into the desired location:

`git clone git@gitlab.com:goodgoodwork/democratscom/democrats.git`

Move into that location:

`cd democrats/`

Now install Wordpress inside the repo (don't worry the `.gitignore` will ignore everything).

### Install WP the easy way

If you have [Wordpress CLI (Command Line Interface)](http://wp-cli.org/)
installed you simply need to run:

`wp core download`

Boom, skip to [run composer step](#run-composer)

### Install WP the hard way

You can download Wordpress core from [wordpress.org/latest.zip](https://wordpress.org/latest.zip).

For this example we'll use command line program `wget` like so:

`wget https://wordpress.org/latest.zip`

Now you'll have `latest.zip` in your project folder, unzip it:

`unzip latest.zip`

This will expand the Wordpress core into `/wordpress/` directory.
Let's move all the content out of that directory into root:

`mv wordpress/* .`

That's `mv` for move, then we are saying 'all content inside wordpress directory' with
the `*` astrisk wildcard. Finally we state the destination as `.` period, which means
'this current directory'.

Finally remove the now empty wordpress folder:

`rm -rf wordpress/`

## Run Composer

Now that Wordpress core is in place you can run the composer install.

If you are new to composer [check out their documentation](https://getcomposer.org/doc/00-intro.md) otherwise
run the install:

`php composer.phar install`

If you've setup command line you can just run:

`composer install`

That's all, now your Wordpress is ready to be configured and setup as normal.

Note: the composer file contains development requirements that shouldn't be installed on production environments. When installing on production, use the `--no-dev` flag.

```
composer install --no-dev
```

## Add Missing Theme

Currently composer isn't playing well with the democrats.com theme, so you'll need to clone it by hand.
Move to the theme folder:

`mv wp-content/themes`

then clone in the theme:

`git clone git@gitlab.com:goodgoodwork/democratscom/democrats-theme.git`
